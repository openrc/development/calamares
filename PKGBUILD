# Maintainer: OpenStage Linux <openstagelinux@gmail.com>
pkgbase=calamares-framework
pkgname=('calamares' 'calamares-settings')
pkgver=3.2.54.openstage
pkgrel=1
pkgdesc='Distribution-independent installer framework'
arch=('x86_64')
license=(GPL)
groups=(build-iso)
url="https://github.com/$pkgname/$pkgname/releases/download/v${pkgver%.*}"
patch_url="https://gitlab.com/openrc/development/calamares-framework/-/raw/development/calamares-patches"
license=('LGPL')
depends=('yaml-cpp' 'kpmcore>=3.1.0' 'boost-libs' 'hwinfo' 'gtk-update-icon-cache'
          'libpwquality' 'polkit-qt5' 'qt5-xmlpatterns' 'plasma-framework')
makedepends=('extra-cmake-modules' 'qt5-tools' 'qt5-translations' 'boost' 'git')
source=(
        "$url/calamares-${pkgver%.*}.tar.gz"
        "$patch_url/services-dinit.patch"
        "$patch_url/services-runit.patch"
        "$patch_url/services-s6.patch"
        "$patch_url/services-suite66.patch"
        "$patch_url/0001-postcfg-module.patch"
        "git+https://gitlab.com/openrc/development/calamares-settings.git#branch=development"
)
md5sums=('909c86334732b4ba933aea4b3accdb7f'
         'd614f36391fb37f881d09acee1fdffd2'
         '53db6c8e8908b217f82c139c8d36044f'
         'beb1f21beba79e3de891eab894945995'
         '7035c89edf69045b7c66b92af75de154'
         '2b1b41d1375565a6b2e1ed21cf9adcfe'
         'SKIP')

prepare() {
#    cp -r $srcdir/calamares-settings/configs/* $srcdir/$pkgname-${pkgver%.*}
    #patch -Np 1 -i $srcdir/*.patch
    cd $srcdir/$pkgname-${pkgver%.*}

    git init
    git add .
    git commit -m "initial"

    git apply ../*.patch
}

build() {
	cd $pkgname-${pkgver%.*}

	mkdir -p build
	cd build
        cmake .. \
              -DCMAKE_BUILD_TYPE=Release \
              -DCMAKE_INSTALL_PREFIX=/usr \
              -DCMAKE_INSTALL_LIBDIR=lib \
              -DWITH_PYTHONQT:BOOL=OFF \
              -DINSTALL_CONFIG:BOOL=ON \
              -DINSTALL_POLKIT:BOOL=ON \
              -DBoost_NO_BOOST_CMAKE=ON \
              -DSKIP_MODULES="webview interactiveterminal initramfs \
                              initramfscfg services-systemd \
                              dummyprocess dummypython dummycpp dummypythonqt"
        make
}

package_calamares() {
	cd $pkgname-${pkgver%.*}/build
	make DESTDIR="$pkgdir" install
}
package_calamares-settings() {
  depends=('calamares')
  mkdir -p {$pkgdir/usr/share/calamares/branding,$pkgdir/etc/calamares}
  cp -r $srcdir/calamares-settings/etc/{settings.conf,modules} $pkgdir/etc/calamares
  cp -r $srcdir/calamares-settings/configs/branding/openstage $pkgdir/usr/share/calamares/branding
}
